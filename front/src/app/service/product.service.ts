import { Injectable } from '@angular/core';
import { Product } from 'app/model/product';
import ProductData from '../../assets/products.json';


@Injectable()
export class ProductService {

  private productList: Product[] = ProductData.data;

  getProductList() {
    return this.productList;
  }

}
