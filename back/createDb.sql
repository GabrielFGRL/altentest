CREATE DATABASE IF NOT EXISTS altendb;

USE altendb;

CREATE TABLE IF NOT EXISTS Product (
     id INT PRIMARY KEY,
     code VARCHAR(255),
     name VARCHAR(255),
     description VARCHAR(255),
     price DECIMAL(10, 2),
     quantity INT,
     inventoryStatus VARCHAR(50),
     category VARCHAR(50),
     image VARCHAR(255),
     rating INT
 );

INSERT INTO Product (id, code, name, description, price, quantity, inventoryStatus, category, image, rating)
VALUES
(1000, 'f230fh0g3', 'Bamboo Watch', 'Product Description', 65, 24, 'INSTOCK', 'Accessories', 'bamboo-watch.jpg', 5),
(1001, 'nvklal433', 'Black Watch', 'Product Description', 72, 61, 'INSTOCK', 'Accessories', 'black-watch.jpg', 4),
(1002, 'zz21cz3c1', 'Blue Band', 'Product Description', 79, 2, 'LOWSTOCK', 'Fitness', 'blue-band.jpg', 3),
(1003, '244wgerg2', 'Blue T-Shirt', 'Product Description', 29, 25, 'INSTOCK', 'Clothing', 'blue-t-shirt.jpg', 5),
(1004, 'h456wer53', 'Bracelet', 'Product Description', 15, 73, 'INSTOCK', 'Accessories', 'bracelet.jpg', 4),
(1005, 'av2231fwg', 'Brown Purse', 'Product Description', 120, 0, 'OUTOFSTOCK', 'Accessories', 'brown-purse.jpg', 4),
(1006, 'bib36pfvm', 'Chakra Bracelet', 'Product Description', 32, 5, 'LOWSTOCK', 'Accessories', 'chakra-bracelet.jpg', 3),
(1007, 'mbvjkgip5', 'Galaxy Earrings', 'Product Description', 34, 23, 'INSTOCK', 'Accessories', 'galaxy-earrings.jpg', 5),
(1008, 'vbb124btr', 'Game Controller', 'Product Description', 99, 2, 'LOWSTOCK', 'Electronics', 'game-controller.jpg', 4),
(1009, 'cm230f032', 'Gaming Set', 'Product Description', 299, 63, 'INSTOCK', 'Electronics', 'gaming-set.jpg', 3),
(1010, 'plb34234v', 'Gold Phone Case', 'Product Description', 24, 0, 'OUTOFSTOCK', 'Accessories', 'gold-phone-case.jpg', 4),
(1011, '4920nnc2d', 'Green Earbuds', 'Product Description', 89, 23, 'INSTOCK', 'Electronics', 'green-earbuds.jpg', 4),
(1012, '250vm23cc', 'Green T-Shirt', 'Product Description', 49, 74, 'INSTOCK', 'Clothing', 'green-t-shirt.jpg', 5),
(1013, 'fldsmn31b', 'Grey T-Shirt', 'Product Description', 48, 0, 'OUTOFSTOCK', 'Clothing', 'grey-t-shirt.jpg', 3),
(1014, 'waas1x2as', 'Headphones', 'Product Description', 175, 8, 'LOWSTOCK', 'Electronics', 'headphones.jpg', 5),
(1015, 'vb34btbg5', 'Light Green T-Shirt', 'Product Description', 49, 34, 'INSTOCK', 'Clothing', 'light-green-t-shirt.jpg', 4),
(1016, 'k8l6j58jl', 'Lime Band', 'Product Description', 79, 12, 'INSTOCK', 'Fitness', 'lime-band.jpg', 3),
(1017, 'v435nn85n', 'Mini Speakers', 'Product Description', 85, 42, 'INSTOCK', 'Clothing', 'mini-speakers.jpg', 4),
(1018, '09zx9c0zc', 'Painted Phone Case', 'Product Description', 56, 41, 'INSTOCK', 'Accessories', 'painted-phone-case.jpg', 5),
(1019, 'mnb5mb2m5', 'Pink Band', 'Product Description', 79, 63, 'INSTOCK', 'Fitness', 'pink-band.jpg', 4),
(1020, 'r23fwf2w3', 'Pink Purse', 'Product Description', 110, 0, 'OUTOFSTOCK', 'Accessories', 'pink-purse.jpg', 4),
(1021, 'pxpzczo23', 'Purple Band', 'Product Description', 79, 6, 'LOWSTOCK', 'Fitness', 'purple-band.jpg', 3),
(1022, '2c42cb5cb', 'Purple Gemstone Necklace', 'Product Description', 45, 62, 'INSTOCK', 'Accessories', 'purple-gemstone-necklace.jpg', 4),
(1023, '5k43kkk23', 'Purple T-Shirt', 'Product Description', 49, 2, 'LOWSTOCK', 'Clothing', 'purple-t-shirt.jpg', 5),
(1024, 'lm2tny2k4', 'Shoes', 'Product Description', 64, 0, 'INSTOCK', 'Clothing', 'shoes.jpg', 4),
(1025, 'nbm5mv45n', 'Sneakers', 'Product Description', 78, 52, 'INSTOCK', 'Clothing', 'sneakers.jpg', 4),
(1026, 'zx23zc42c', 'Teal T-Shirt', 'Product Description', 49, 3, 'LOWSTOCK', 'Clothing', 'teal-t-shirt.jpg', 3),
(1027, 'acvx872gc', 'Yellow Earbuds', 'Product Description', 89, 35, 'INSTOCK', 'Electronics', 'yellow-earbuds.jpg', 3),
(1028, 'tx125ck42', 'Yoga Mat', 'Product Description', 20, 15, 'INSTOCK', 'Fitness', 'yoga-mat.jpg', 5),
(1029, 'gwuby345v', 'Yoga Set', 'Product Description', 20, 25, 'INSTOCK', 'Fitness', 'yoga-set.jpg', 8);